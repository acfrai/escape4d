package e4d.utils;

public class Vec4f {

    private float[] vec = new float[4];

    public Vec4f() {
        this(0, 0, 0, 1.0f);
    }

    public Vec4f(float x, float y, float z) {
        this(x, y, z, 1.0f);
    }

    public Vec4f(float x, float y, float z, float h) {
        vec[0] = x;
        vec[1] = y;
        vec[2] = z;
        vec[3] = h;
    }

    public Vec4f(float[] vector) {
        vec = new float[4];
        System.arraycopy(vector, 0, vec, 0, 4);
    }

    public Vec4f(Vec4f other) {
        System.arraycopy(other.vec, 0, vec, 0, 4);
    }

    public static Vec4f sum(Vec4f c1, Vec4f c2) {
        return new Vec4f(c1.x() + c2.x(), c1.y() + c2.y(), c1.z() + c2.z());
    }

    public Vec4f sum(Vec4f vec) {
        return sum(this, vec);
    }

    public Vec4f divide(float s) {
        return new Vec4f(x() / s, y() / s, z() / s);
    }

    public Vec4f multMatrix4(float[] matrix) {
        float[] result = new float[4];

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                result[i] += vec[j] * matrix[4 * j + i];
            }
        }

        return new Vec4f(result);
    }

    public float[] getFloatv() {
        float[] result = new float[4];
        System.arraycopy(vec, 0, result, 0, 4);
        return result;
    }

    public float x() {
        return vec[0] / vec[3];
    }

    public float y() {
        return vec[1] / vec[3];
    }

    public float z() {
        return vec[2] / vec[3];
    }

    public float magnitude() {
        return (float) Math.sqrt(x() * x() + y() * y() + z() * z());
    }

    @Override
    public String toString() {

        return "(" + x() + "," + y() + "," + z() + ")";
    }

    public static Vec4f sub(Vec4f c1, Vec4f c2) {
        return new Vec4f(c1.x() - c2.x(), c1.y() - c2.y(), c1.z() - c2.z());
    }

    public Vec4f sub(Vec4f vec) {
        return sub(this, vec);
    }

    public void normalize() {
        euclidian();

        float sum = 0.0f;
        for (int i = 0; i < 3; i++) {
            sum += vec[i] * vec[i];
        }
        sum = (float) Math.sqrt(sum);

        for (int i = 0; i < 3; i++) {
            vec[i] = vec[i] / sum;
        }
    }

    private void euclidian() {
        for (int i = 0; i < 3; i++) {
            vec[i] = vec[i] / vec[3];
        }
    }

    public Vec4f mult(float m) {
        return new Vec4f(x() * m, y() * m, z() * m);
    }

    public Vec4f negative() {
        return new Vec4f(-x(), -y(), -z());
    }

    public float distance(Vec4f other) {
        return sub(other).magnitude();
    }

    public boolean isZero() {
        if (x() != 0 || y() != 0 || z() != 0) {
            return false;
        }
        return true;
    }

    public boolean isZeroEnough() {
        if (x() < 0.1f && y() < 0.1f && z() < 0.1f
                && x() > -.1f && y() > -.1f && z() > -.1f) {
            return true;
        }
        return false;
    }

    public float at(int i) {
        return vec[i];
    }
}
