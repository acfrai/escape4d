package e4d.utils;

import e4d.object.Model;
import java.util.HashSet;
import java.util.Set;

public class Octree {

    private Vec4f corner1, corner2, center;
    private Octree[][][] children = new Octree[2][2][2];
    private Set<Model> objects = new HashSet<Model>();
    private boolean hasChildren;
    private int depth;
    private int numberOfObjects;

    public int MIN_OBJECTS_PER_OCTREE = 3;
    public int MAX_OCTREE_DEPTH = 6;
    public int MAX_OBJECTS_PER_OCTREE = 6;

    //Adds a ball to or removes one from the children of this
    private void fileObject(Model obj, boolean addObj) {
        //Figure out in which child(ren) the ball belongs
        for (int x = 0; x < 2; x++) {
            if (x == 0) {
                if (!obj.isOn(Model.ON_LEFT, center)) {
                    continue;
                }
            } else if (!obj.isOn(Model.ON_RIGHT, center)) {
                continue;
            }

            for (int y = 0; y < 2; y++) {
                if (y == 0) {
                    if (!obj.isOn(Model.ON_BOTTOM, center)) {
                        continue;
                    }
                } else if (!obj.isOn(Model.ON_TOP, center)) {
                    continue;
                }

                for (int z = 0; z < 2; z++) {
                    if (z == 0) {
                        if (!obj.isOn(Model.ON_NEAR, center)) {
                            continue;
                        }
                    } else if (!obj.isOn(Model.ON_FAR, center)) {
                        continue;
                    }

                    //Add or remove the ball
                    if (addObj) {
                        children[x][y][z].add(obj);
                    } else {
                        children[x][y][z].remove(obj);
                    }
                }
            }
        }
    }

    private void haveChildren() {
        for (int x = 0; x < 2; x++) {
            float minX;
            float maxX;
            if (x == 0) {
                minX = corner1.x();
                maxX = center.x();
            } else {
                minX = center.x();
                maxX = corner2.x();
            }

            for (int y = 0; y < 2; y++) {
                float minY;
                float maxY;
                if (y == 0) {
                    minY = corner1.y();
                    maxY = center.y();
                } else {
                    minY = center.y();
                    maxY = corner2.y();
                }

                for (int z = 0; z < 2; z++) {
                    float minZ;
                    float maxZ;
                    if (z == 0) {
                        minZ = corner1.z();
                        maxZ = center.z();
                    } else {
                        minZ = center.z();
                        maxZ = corner2.z();
                    }

                    children[x][y][z] = new Octree(new Vec4f(minX, minY, minZ),
                            new Vec4f(maxX, maxY, maxZ),
                            depth + 1);
                }
            }
        }

        //Remove all balls from "balls" and add them to the new children
        for (Model object : objects) {
            fileObject(object, true);
        }
        objects.clear();

        hasChildren = true;
    }

    //Adds all balls in this or one of its descendants to the specified set
    private void collectObjects(Set<Model> bs) {
        if (hasChildren) {
            for (int x = 0; x < 2; x++) {
                for (int y = 0; y < 2; y++) {
                    for (int z = 0; z < 2; z++) {
                        children[x][y][z].collectObjects(bs);
                    }
                }
            }
        } else {
            for (Model object : objects) {
                bs.add(object);
            }
        }
    }

    //Destroys the children of this, and moves all balls in its descendants
    //to the "balls" set
    private void destroyChildren() {
        collectObjects(objects);

        for (int x = 0; x < 2; x++) {
            for (int y = 0; y < 2; y++) {
                for (int z = 0; z < 2; z++) {
                    children[x][y][z] = null;
                }
            }
        }

        hasChildren = false;
    }

    //Removes the specified ball at the indicated position
    public void remove(Model obj) {
        numberOfObjects--;

        if (hasChildren && numberOfObjects < MIN_OBJECTS_PER_OCTREE) {
            destroyChildren();
        }

        if (hasChildren) {
            fileObject(obj, false);
        } else {
            objects.remove(obj);
        }
    }

    //Constructs a new Octree.  c1 is (minX, minY, minZ), c2 is (maxX, maxY,
    //maxZ), and d is the depth, which starts at 1.
    public Octree(Vec4f c1, Vec4f c2, int d) {
        corner1 = c1;
        corner2 = c2;
        center = Vec4f.sum(c1, c2).divide(2.0f);
        depth = d;
        numberOfObjects = 0;
        hasChildren = false;
    }

    //Adds a ball to this
    public void add(Model obj) {
        numberOfObjects++;
        if (!hasChildren && depth < MAX_OCTREE_DEPTH
                && numberOfObjects > MAX_OBJECTS_PER_OCTREE) {
            haveChildren();
        }

        if (hasChildren) {
            fileObject(obj, true);
        } else {
            objects.add(obj);
        }
    }
    
    public Set<Model> potencialCollisions(Model obj) {
        Set<Model> set = new HashSet<Model>();
        
        if(!hasChildren) {
            set.addAll(objects);
            return set;
        }
        
        for (int x = 0; x < 2; x++) {
            if (x == 0) {
                if (!obj.isOn(Model.ON_LEFT, center)) {
                    continue;
                }
            } else if (!obj.isOn(Model.ON_RIGHT, center)) {
                continue;
            }

            for (int y = 0; y < 2; y++) {
                if (y == 0) {
                    if (!obj.isOn(Model.ON_BOTTOM, center)) {
                        continue;
                    }
                } else if (!obj.isOn(Model.ON_TOP, center)) {
                    continue;
                }

                for (int z = 0; z < 2; z++) {
                    if (z == 0) {
                        if (!obj.isOn(Model.ON_NEAR, center)) {
                            continue;
                        }
                    } else if (!obj.isOn(Model.ON_FAR, center)) {
                        continue;
                    }

                    set.addAll(children[x][y][z].potencialCollisions(obj));
                }
            }
        }
        
        return set;
    }
}
