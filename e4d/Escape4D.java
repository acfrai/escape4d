package e4d;

import com.sun.opengl.util.Animator;
import com.sun.opengl.util.FPSAnimator;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLCapabilities;
import javax.swing.Icon;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Escape4D {

    public static JFrame frame;
    public static Icon image;
    public static JPanel label;
    public static JFrame loading;
    
    
    public static void main(String[] args) {
        GLCapabilities caps = new GLCapabilities();
        caps.setDoubleBuffered(true);
        caps.setHardwareAccelerated(true);

        Listener listener = new Listener();
  //      Listener listenerLoading = new Listener();
        
        GLCanvas canvas = new GLCanvas(caps);
//        GLCanvas canvasLoading = new GLCanvas(caps);
        
        canvas.addGLEventListener(listener);
//        canvasLoading.addGLEventListener(listenerLoading);

        /* Tentativa de fazer a tela de loading
         * nao funciona AINDA
         */
//        image = new ImageIcon("loading.jpg");
//        label = new JPanel();
//        loading = new JFrame("Loading Escape4D");
//        loading.getContentPane().add(label);
//        loading.setLocationRelativeTo(null);
//        loading.setSize(800, 600);
//        loading.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        frame = new JFrame("Escape4D");
        frame.getContentPane().add(canvas);
        frame.setSize(800, 600);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
  //      loading.setVisible(true);
        canvas.addKeyListener(listener);

        Animator animator = new FPSAnimator(canvas, 24);
        
        animator.start();
    }
}