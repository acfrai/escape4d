package e4d.core;

import e4d.map.Maze;
import e4d.ninja.NinjaArm;
import e4d.ninja.NinjaHead;
import e4d.ninja.NinjaLeg;
import e4d.object.AnimatedModel;
import e4d.object.Model;
import e4d.utils.Octree;
import e4d.utils.Vec4f;
import java.util.Iterator;
import java.util.Set;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.glu.GLU;

public class Ninja extends AnimatedModel {

    private float time_S2R = 500.0f;
    private float[] rel_S2R = {0.48f, 1.44f, 1.92f};
    private int wait;
    private int side;
    //
    public static final int LEFT = 0;
    public static final int RIGHT = 1;
    private final NinjaHead head = new NinjaHead(drawable);
    private final NinjaArm lArm = new NinjaArm(drawable, LEFT, 0);
    private final NinjaArm rArm = new NinjaArm(drawable, RIGHT, 1);
    private final NinjaLeg lLeg = new NinjaLeg(drawable, LEFT, 0);
    private final NinjaLeg rLeg = new NinjaLeg(drawable, RIGHT, 1);
    private Octree octree;
    //
    private Vec4f direction;
    private Vec4f position;
    private Vec4f viewUp;
    private float angle;
    //
    private Vec4f direction0;
    private Vec4f position0;
    private float angle0;
    //
    private boolean rotation;
    private float[] body_modelview;
    private float[] mirror;
    //
    private float offsetUp;
    private float offsetFront;
    private float offsetFP;

    public Ninja(GLAutoDrawable drawable) {
        super("./data/big_man_body_t.obj", drawable, 0);

        // Body
        body_modelview = new float[]{0.43000054f, 0.0f, 0.0f, 0.0f,
            0.0f, 0.43000054f, 0.0f, 0.0f,
            0.0f, 0.0f, 0.43000054f, 0.0f,
            0.0f, 0.90499976f, -0.015f, 1.0f};
        multMatrix(body_modelview, 0);

        mirror = new float[]{
            1, 0, 0, 0,
            0, -1, 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1
        };

        rotation = false;

        addChild(head);
        addChild(lArm);
        addChild(rArm);
        addChild(lLeg);
        addChild(rLeg);

        octree = null;
        offsetFront = .1f;

        requestState(Game.STATE_STAND);
    }

    @Override
    protected void animation() {

        GL gl = drawable.getGL();
        long ticks = getTicks();

        switch (state) {
            case Game.STATE_STAND:
                if (!nextStates.isEmpty()) {
                    transition(state, nextStates.remove(0));
                }
                break;

            case Game.STATE_STAND2RUNNING:
                state = Game.STATE_RUNNING;
                if (side == Ninja.RIGHT) {
                    wait = (int) time_S2R / 2;
                }
                clearTicks();
                break;

            case Game.STATE_RUNNING:
            case Game.STATE_RUNNING2STAND:
                float degrees = 0;

                if (ticks < wait) {
                    return;
                } else if (wait > 0) {
                    wait = -1;
                    clearTicks();
                    return;
                }

                if (ticks <= time_S2R * rel_S2R[0]) {

                    degrees = -5.0f * ((ticks) / (time_S2R * rel_S2R[0]));

                } else if (ticks <= time_S2R * rel_S2R[1]) {
                    degrees = -5.0f + 10.0f * ((ticks - (time_S2R * rel_S2R[0])) / (time_S2R * rel_S2R[1] - time_S2R * rel_S2R[0]));


                } else if (ticks <= time_S2R * rel_S2R[2]) {
                    degrees = 5 - 5.0f * ((ticks - (time_S2R * rel_S2R[1])) / (time_S2R * rel_S2R[2] - time_S2R * rel_S2R[1]));

                } else {
                    clearTicks();
                    if (state == Game.STATE_RUNNING2STAND) {
                        state = Game.STATE_STAND;
                        return;
                    }
                }

                gl.glTranslatef(0.0f, 0.25f, 0.0f);
                gl.glRotatef(degrees, 0, 1, 1);
                gl.glTranslatef(0.0f, -0.25f, 0.0f);

                if (!nextStates.isEmpty()) {
                    int lState = state;
                    transition(state, nextStates.remove(0));
                    while (state == lState && !nextStates.isEmpty()) {
                        transition(state, nextStates.remove(0));
                    }
                }

                break;

            default:
                if (!nextStates.isEmpty()) {
                    state = nextStates.remove(0);
                }

        }
    }

    public void setOctree(Octree octree) {
        this.octree = octree;
    }

    public void move(float m) {
        Vec4f tmp = new Vec4f(position);
        float[] lastModelview = getModelview();

        position = position.sum(direction.mult(m));

        setPosition();

        if (calcCollision()) {
            position = tmp;
        }

        reset();
        multMatrix(lastModelview, 0);
    }
    
    public void turn(float t) {
        GL gl = drawable.getGL();

        gl.glMatrixMode(GL.GL_MODELVIEW);
        gl.glPushMatrix();

        gl.glLoadIdentity();
        gl.glRotatef(t, viewUp.x(), viewUp.y(), viewUp.z());

        float[] matrix = new float[16];
        gl.glGetFloatv(GL.GL_MODELVIEW_MATRIX, matrix, 0);

        gl.glPopMatrix();

        direction = direction.multMatrix4(matrix);
        direction.normalize();

        angle += t;
        if (angle > 360) {
            angle -= 360;
        } else if (angle < -360) {
            angle += 360;
        }
    }

    public void flip() {
        rotation = !rotation;

        setPosition();
        if(calcCollision()) {
            rotation = !rotation;
        }
        
        setPosition0();
    }

    public void setMaze(Maze maze) {
        direction = maze.getFront();
        position = maze.getInitialPosition();
        viewUp = maze.getViewUp();

        direction0 = maze.getFront();
        position0 = maze.getInitialPosition();
        viewUp = maze.getViewUp();

        setPosition0();
    }

    private boolean calcCollision() {
        Set<Model> models = octree.potencialCollisions(this);

        for (Iterator<Model> it = models.iterator(); it.hasNext();) {
            Model m = it.next();
            if (collides(m)) {
                return true;
            }
        }

        return false;
    }

    public void lookMe() {
        update();

        GLU glu = new GLU();
        GL gl = drawable.getGL();

        gl.glMatrixMode(GL.GL_MODELVIEW);
        if (!rotation) {
            glu.gluLookAt(position0.x() - direction0.x()*offsetFront, 0.0f, position0.z() - direction.z()*offsetFront,
                    position0.x(), position0.y() + 1 + offsetUp, position0.z(),
                    viewUp.x(), viewUp.y(), viewUp.z());
        }
        else {
            glu.gluLookAt(position0.x() - direction0.x()*offsetFront, 0.0f, position0.z() - direction.z()*offsetFront,
                    position0.x(), position0.y() + 2.0f + offsetUp, position0.z(),
                    viewUp.x(), viewUp.y(), viewUp.z());
        }
    }

    private void update() {
        boolean request = false;

        if (!position.sub(position0).isZeroEnough()) {
            request = true;
            // p0 = p0 + (p - p0)
            position0 = position0.sum(position.sub(position0).mult(.3f));
        }

        if (angle0 != angle) {
            request = true;
            angle0 = angle0 + (angle - angle0) * 0.9f;
        }

        if (!direction.sub(direction0).isZeroEnough()) {
            request = true;
            direction0 = direction0.sum(direction.sub(direction0).mult(0.3f));
        }

        if (request) {
            requestState(Game.STATE_RUNNING);
            setPosition0();
        } else {
            requestState(Game.STATE_STAND);
        }
        
        if(offsetFront != 1) {
            offsetFront = offsetFront + 0.05f*(1 - offsetFront);
        }
        
        if(offsetUp != 0) {
            offsetUp = offsetUp - 0.05f*offsetUp;
        }
        
        
    }

    private void setPosition0() {
        reset();
        translate(position0);
        if (rotation) {
            translate(0.0f, -position0.y(), 0.0f);
            rotate(180, direction0);
            translate(0.0f, position0.y(), 0.0f);
        }
        rotate(angle0, viewUp.x(), viewUp.y(), viewUp.z());
        multMatrix(body_modelview, 0);
    }

    private void setPosition() {
        reset();
        translate(position);
        if (rotation) {
            translate(0.0f, -position.y(), 0.0f);
            rotate(180, direction);
            translate(0.0f, position.y(), 0.0f);
        }
        rotate(angle, viewUp.x(), viewUp.y(), viewUp.z());
        multMatrix(body_modelview, 0);
    }

    void setCameraEvent(int event) {
        switch(event) {
            case Game.EVENT_CAM_BACK:
                offsetFront = 1.5f;
                break;
            case Game.EVENT_CAM_FRONT:
                offsetFront = .5f;
                break;
            case Game.EVENT_CAM_UP:
                offsetUp = .5f;
                break;
            case Game.EVENT_CAM_DOWN:
                offsetUp = -.5f;
                break;
            case Game.EVENT_CAM_FIRST_PERSON:
                break;
        }
    }
}
