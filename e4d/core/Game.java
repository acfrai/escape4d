package e4d.core;

import e4d.Escape4D;
import e4d.object.Model;
import java.awt.Frame;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Iterator;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.glu.GLU;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class Game {

    public static final int STATE_STAND = 2;
    public static final int STATE_RUNNING = 4;
    public static final int STATE_STAND2RUNNING = 6;
    public static final int STATE_RUNNING2STAND = 7;
    private static Game instance = null;
    private Ninja ninja;
    private World world;
    private final int EVENT_CLOSE = 0;
    private final int EVENT_MOVE_FORWARD = 1;
    private final int EVENT_MOVE_BACKWARD = 2;
    private final int EVENT_TURN_RIGHT = 3;
    private final int EVENT_TURN_LEFT = 4;
    private final int EVENT_FLIP = 5;
    private final int EVENT_SWITCHFOG = 6;    
    public static final int EVENT_CAM_BACK = 7;
    public static final int EVENT_CAM_FRONT = 8;
    public static final int EVENT_CAM_DOWN = 9;
    public static final int EVENT_CAM_UP = 10;
    public static final int EVENT_CAM_FIRST_PERSON = 11;
    private static final int EVENT_RESET = 12;
    public static final int EVENT_DEBUGGER_MODE = 13;
    public static final int EVENT_SHOW_MASK = 14;
    
    private int width = 640;
    private int height = 480;
    
    private boolean fog;
    private ArrayList<Integer> events = new ArrayList<Integer>();
    private int[] fogMode = {GL.GL_EXP, GL.GL_EXP2, GL.GL_LINEAR};
    private float[] fogColor = {0.35f, 0.35f, 0.35f, 1.0f};
    private int fogFilter;
    
    /* Debugger Variables */
    public static boolean debugger = false;
    public float alpha = 0.0f;
    public float beta = 0.0f;
    public float delta = 1.0f;
    private float gama = 0.0f;
    public float p0x = 0.0f;
    public float p0y = 0.0f;
    public float p0z = 1.0f;
    public float pRefx = 0.0f;
    public float pRefy = 0.0f;
    public float pRefz = 0.0f;
    public float vX = 0.0f;
    public float vY = 1.0f;
    public float vZ = 0.0f;
    private boolean showMask = false;

    
    /*public ImageIcon image;
    public JLabel label;
    public JFrame loading;*/
       
    protected Game() {
    
    }

    public static Game getInstance() {
        if (instance == null) {
            instance = new Game();
        }
        return instance;
    }

    public void init(GLAutoDrawable drawable) {
        /*this.image = new ImageIcon("loading.jpg");
        this.label = new JLabel(this.image);
        this.loading = new JFrame("Loading Escape4D");
        this.loading.getContentPane().add(this.label);
        this.loading.setSize(400, 435);
        this.loading.setLocationRelativeTo(null);
        this.loading.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.loading.setVisible(true);*/
        
        GL gl = drawable.getGL();
        GLU glu = new GLU();

        // Recursos
        gl.glEnable(GL.GL_DEPTH_TEST);
        gl.glEnable(GL.GL_CULL_FACE);
        gl.glCullFace(GL.GL_BACK);

        gl.glMatrixMode(GL.GL_MODELVIEW);
        gl.glLoadIdentity();

        gl.glMatrixMode(GL.GL_PROJECTION);
        gl.glLoadIdentity();
        glu.gluPerspective(45.0f, 1.3f, 0.005f, -0.005f);

        gl.glShadeModel(GL.GL_SMOOTH);
        gl.glClearDepth(10.0f); // Depth Buffer Setup 
        gl.glEnable(GL.GL_DEPTH_TEST); // Enables Depth Testing
        gl.glDepthFunc(GL.GL_LEQUAL); // The Type Of Depth Testing To Do
        gl.glHint(GL.GL_PERSPECTIVE_CORRECTION_HINT, GL.GL_NICEST);	// Really Nice Perspective Calculations

        float[] lightColorAmbient1 = {0.2f, 0.2f, 0.2f, 1f};
        float[] lightColorSpecular1 = {0.5f, 0.5f, 0.5f, 1f};
        float[] lightColorDiffuse1 = {0.5f, 0.5f, 0.5f, 1f};
        float[] lightPos1 = {0, 0, 0, 1.0f};
        
        gl.glLightModelfv(GL.GL_LIGHT_MODEL_AMBIENT, lightColorAmbient1, 0);
        gl.glLightfv(GL.GL_LIGHT1, GL.GL_AMBIENT, lightColorAmbient1, 0);
        gl.glLightfv(GL.GL_LIGHT1, GL.GL_SPECULAR, lightColorSpecular1, 0);
        gl.glLightfv(GL.GL_LIGHT1, GL.GL_DIFFUSE, lightColorDiffuse1, 0);
        gl.glLightfv(GL.GL_LIGHT1, GL.GL_POSITION, lightPos1, 0);

        gl.glEnable(GL.GL_LIGHTING);
        gl.glEnable(GL.GL_LIGHT1);

        gl.glClearColor(fogColor[0], fogColor[1], fogColor[2], fogColor[3]);
        gl.glFogi(GL.GL_FOG_MODE, fogMode[fogFilter]);
        gl.glFogfv(GL.GL_FOG_COLOR, fogColor, 0);
        gl.glFogf(GL.GL_FOG_DENSITY, 0.75f);
        gl.glHint(GL.GL_FOG_HINT, GL.GL_NICEST);
        gl.glFogf(GL.GL_FOG_START, 0.005f);
        gl.glFogf(GL.GL_FOG_END, 5.0f);
        gl.glEnable(GL.GL_FOG);
        fog = true;

        Model.drawCollisionMask = false;

        ninja = new Ninja(drawable);
        world = new World(ninja, width, height, drawable);
    }

    public void update(GLAutoDrawable drawable) {
        // Processar eventos
        readEvents(drawable);

        GL gl = drawable.getGL();
        GLU glu = new GLU();
        
        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

        gl.glViewport(0, 0, width, height);
        gl.glMatrixMode(GL.GL_PROJECTION);
        gl.glLoadIdentity();
        
        glu.gluPerspective(45.0f, ((float) this.width) / ((float) this.height), 0.005f, -0.005f);        
        
        gl.glMatrixMode(GL.GL_MODELVIEW);
        gl.glLoadIdentity();

        /* Debugger Mode */
        if(debugger) {
            if(!showMask) glu.gluLookAt(p0x, p0y, p0z, pRefx, pRefy, pRefz, vX, vY, vZ);
            gl.glScalef(delta, delta, delta);
            gl.glRotatef(alpha, 1, 0, 0);
            gl.glRotatef(beta, 0, 1, 0);
            gl.glRotatef(gama, 0, 0, 1);
            gl.glDisable(GL.GL_FOG);
            gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        }
        else if(fog) {
            gl.glEnable(GL.GL_FOG);
            gl.glClearColor(fogColor[0], fogColor[1], fogColor[2], fogColor[3]);
        }
               
        world.reset();
        ninja.lookMe();
        world.draw();
        world.minimap();
        gl.glFlush();
    }

    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        GL gl = drawable.getGL();
        GLU glu = new GLU();
        float w, h;

        // Evita a divisao por zero
        if (height == 0) {
            this.height = 1;
        } else {
            this.height = height;
        }
        this.width = width;

        gl.glMatrixMode(GL.GL_PROJECTION);
        gl.glLoadIdentity();
        glu.gluPerspective(45.0f, ((float) this.width) / ((float) this.height), 0.005f, -0.005f);
        
        world.setWindowSize(this.width, this.height);
    }

    private void readEvents(GLAutoDrawable drawable) {
        GL gl = drawable.getGL();
        if (events.isEmpty() == false) {
            int event = events.remove(0);

            if (event == EVENT_CLOSE) {
                System.exit(0);
            } else if (event == EVENT_FLIP) {
                ninja.flip();

            } else if (event >= EVENT_MOVE_FORWARD && event <= EVENT_TURN_LEFT) {
                events.add(event);
                float m = 0.0f, t = 0.0f;
                ArrayList<Integer> tmp = new ArrayList<Integer>();

                Iterator<Integer> it = events.iterator();
                int i;

                while (it.hasNext()) {
                    i = it.next();
                    switch (i) {
                        case EVENT_MOVE_FORWARD:
                            m += 0.05f;
                            break;
                        case EVENT_MOVE_BACKWARD:
                            m -= 0.05f;
                            break;
                        case EVENT_TURN_LEFT:
                            t += 3.0f;
                            break;
                        case EVENT_TURN_RIGHT:
                            t -= 3.0f;
                            break;
                        default:
                            tmp.add(i);
                    }
                }

                events.clear();
                events = tmp;
                
                if(m > .8f) {
                    m = .8f;
                } else if(m < -.8f) {
                    m = -.8f;
                }

                ninja.move(m);
                ninja.turn(t);
            } else if (event == EVENT_SWITCHFOG) {
                fog = !fog;
                if (fog) {
                    gl.glEnable(GL.GL_FOG);
                } else {
                    gl.glDisable(GL.GL_FOG);
                }
            } else if(event >= EVENT_CAM_BACK && event <= EVENT_CAM_UP) {
                ninja.setCameraEvent(event);
            } else if(event == EVENT_CAM_FIRST_PERSON) {
                ninja.setCameraEvent(event);
            } else if(event == EVENT_RESET) {
                this.init(drawable);
            } else if(event == EVENT_DEBUGGER_MODE) {
                debugger = !debugger;
            } else if(event == EVENT_SHOW_MASK) {
                showMask = !showMask;
            }
            
        }
    }

    public void addKey(int e) {
        switch (e) {
            case KeyEvent.VK_ESCAPE:
                events.add(EVENT_CLOSE);
                break;
            case KeyEvent.VK_W:
                events.add(EVENT_MOVE_FORWARD);
                break;
            case KeyEvent.VK_S:
                events.add(EVENT_MOVE_BACKWARD);
                break;
            case KeyEvent.VK_A:
                events.add(EVENT_TURN_LEFT);
                break;
            case KeyEvent.VK_D:
                events.add(EVENT_TURN_RIGHT);
                break;
            case KeyEvent.VK_SPACE:
                events.add(EVENT_FLIP);
                break;
            case KeyEvent.VK_F:
                events.add(EVENT_SWITCHFOG);
                break;
            case KeyEvent.VK_LEFT:
                events.add(EVENT_CAM_BACK);
                break;
            case KeyEvent.VK_RIGHT:
                events.add(EVENT_CAM_FRONT);
                break;
            case KeyEvent.VK_UP:
                events.add(EVENT_CAM_UP);
                break;
            case KeyEvent.VK_DOWN:
                events.add(EVENT_CAM_DOWN);
                break;
            case KeyEvent.VK_R:
                events.add(EVENT_RESET);
                break;
            case KeyEvent.VK_M:
                events.add(EVENT_SHOW_MASK);
                break;
            case KeyEvent.VK_B:
                events.add(EVENT_DEBUGGER_MODE);
                break;
            case KeyEvent.VK_PAGE_UP://faz zoomin
                delta = delta * 1.1f;
                break;
            case KeyEvent.VK_PAGE_DOWN://faz zoomout
                delta = delta * 0.809f;
                break;
            case KeyEvent.VK_I://gira sobre o eixox
                alpha = alpha - 1;
                break;
            case KeyEvent.VK_K://gira sobre o eixox
                alpha = alpha + 1;
                break;
            case KeyEvent.VK_J://gira sobre o eixoy
                beta = beta + 1;
                break;
            case KeyEvent.VK_L://gira sobre o eixoy
                beta = beta - 1;
                break;
            case KeyEvent.VK_Y://gira sobre o eixoy
                gama = gama + 1;
                break;
            case KeyEvent.VK_H://gira sobre o eixoy
                gama = gama - 1;
                break;
        }
    }
}
