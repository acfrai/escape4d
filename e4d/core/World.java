package e4d.core;

import com.sun.opengl.util.GLUT;
import e4d.map.Maze;
import e4d.object.Model;
import e4d.utils.Vec4f;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.glu.GLU;

public class World extends Model {

    private Maze maze;
    private int width;
    private int height;
    private Ninja player;
    private Model miniMap = new Model("./data/wall/minimap.obj", drawable, 0);
    
    // Jamais mude a modelview do World
    // sem atualizar a Octree :D
    public World(Ninja player, int width, int height, GLAutoDrawable drawable) {
        super(drawable);

        maze = new Maze("./data/sample1.e4d", drawable);
        maze.setPosition(player);
        
        this.width = width;
        this.height = height;
        
        player.setMaze(maze);
        player.setOctree(maze.getOctree());

        addChild(player);
        addChild(maze);
        
        size = 0.0f;
        
        this.player = player;
    }
    
    public void minimap() {
        GL gl = drawable.getGL();
        GLU glu = new GLU();
        GLUT glut = new GLUT();
        
        if(width <= 400 || height <= 400) return;
        
        gl.glViewport(width - 150, 0, 150, 150);
        
        gl.glMatrixMode(GL.GL_PROJECTION);
        gl.glLoadIdentity();
        gl.glOrtho(-maze.getLength(), maze.getLength(), -maze.getWidth(), maze.getWidth(), 0.0f, 2.0f * maze.getHeight());

        gl.glMatrixMode(GL.GL_MODELVIEW);
        gl.glLoadIdentity();

        glu.gluLookAt(0.0f, maze.getHeight(), 0.0f,
                0.0f, -maze.getHeight(), 0.0f,
                0.0f, 0.0f, 1.0f);

        gl.glColor3f(1.0f, 0.0f, 0.0f);
        Vec4f center = player.getCenter();
        gl.glTranslatef(center.x(), 0.0f, center.z());
        glut.glutSolidSphere(0.5f, 20, 20);
        
        gl.glColor3f(0.0f, 1.0f, 0.0f);
        gl.glTranslatef(-center.x(), 0.0f, -center.z());
        gl.glPushMatrix();
        gl.glScalef(maze.getLength(), 0.05f, maze.getWidth());
        miniMap.draw();
        gl.glPopMatrix();
        gl.glTranslatef((maze.getLength() / 2.0f) + (maze.getLength() / (0.5f * maze.getPartitions())),
                0.0f, (maze.getWidth() / 2.0f) +  (maze.getWidth() / (0.5f * maze.getPartitions())));
        gl.glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
        
        glut.glutSolidTorus(0.3f, 0.4f, 20, 20);        
    }

    @Override
    protected void drawing() {
    }

    void setWindowSize(int width, int height) {
        this.width = width;
        this.height = height;
    }
}
