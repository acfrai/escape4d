package e4d.map;

import com.sun.opengl.util.GLUT;
import e4d.object.Model;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;

public class Floor extends Model {
    
    private int i;
    private int j;
    private int k;
    private Maze maze;
        
    public Floor(int[] map, Maze maze, GLAutoDrawable drawable) {
        super("./data/floor/floor1.obj", drawable, 0);
        
        this.i = map[0];
        this.j = map[1];
        this.k = map[2];
        this.maze = maze;
        
        translate(0.0f, ((k == 2) ? (maze.getHeight()) : (-maze.getHeight())), 0.0f);
        scale(maze.getLength(), maze.getHeight() / 2 * 0.05f, maze.getWidth());
        
        size = 0.0f;
    }

    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }

    public int getJ() {
        return j;
    }

    public void setJ(int j) {
        this.j = j;
    }

    public int getK() {
        return k;
    }

    public void setK(int k) {
        this.k = k;
    }
}