package e4d.map;

import e4d.core.Game;
import e4d.object.Model;
import e4d.utils.Octree;
import e4d.utils.Vec4f;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Set;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;

public class Maze extends Model {

    private float width;
    private float height;
    private float length;
    private int partitions;
    private int numWalls;
    private int numSections;
    private int[] map = new int[6];
    private ArrayList<WallTexturized> walls;
    private ArrayList<Floor> floors;
    private Octree octree;
    private Model pivot;
    private final int NORMAL = 0;
    private final int DOWN = 1;
    private final int UP = 2;
    private final int YGD = 3;
    private final int HELLPME = 4;
    private final int GHOST = 5;
    private final int IWPG = 6;
    private final int END = 7;

    public Maze(String file, GLAutoDrawable drawable) {
        super(drawable);

        Floor floor;
        WallTexturized wall;

        try {
            Scanner s = new Scanner(new File(file));

            this.length = s.nextInt();
            this.width = s.nextInt();
            this.height = s.nextInt();
            this.partitions = s.nextInt();
            this.numWalls = s.nextInt();
            this.numSections = s.nextInt();

            Vec4f min = new Vec4f((this.length / 2.0f), -this.height, (-this.width / 2.0f));
            Vec4f max = new Vec4f((25.0f * this.length), 60 * this.height, (25.0f * this.width));

            octree = new Octree(min, max, 1);
            octree.MIN_OBJECTS_PER_OCTREE = 6;
            octree.MAX_OBJECTS_PER_OCTREE = 12;
            octree.MAX_OCTREE_DEPTH = 30;

            floors = new ArrayList<Floor>();
            walls = new ArrayList<WallTexturized>();

            for (int i = 0; i < 2; i++) {
                this.map[0] = s.nextInt();
                this.map[1] = s.nextInt();
                this.map[2] = s.nextInt();
                this.map[3] = s.nextInt();

                floor = new Floor(map, this, drawable);
                floors.add(floor);
                addChild(floor);
            }

            String pathFile = null;

            for (int i = 0; i < numWalls; i++) {
                this.map[0] = s.nextInt();
                this.map[1] = s.nextInt();
                this.map[2] = s.nextInt();
                this.map[3] = s.nextInt();
                this.map[4] = s.nextInt();
                this.map[5] = s.nextInt();

                if (NORMAL == this.map[4]) {
                    pathFile = "./data/wall/wall.obj";
                } else if (DOWN == this.map[4]) {
                    pathFile = "./data/wall/wall_down.obj";
                } else if (UP == this.map[4]) {
                    pathFile = "./data/wall/wall_up.obj";
                } else if (YGD == this.map[4]) {
                    pathFile = "./data/wall/wall_ygd.obj";
                } else if (HELLPME == this.map[4]) {
                    pathFile = "./data/wall/wall_hellpme.obj";
                } else if (GHOST == this.map[4]) {
                    pathFile = "./data/wall/wall_ghost.obj";
                } else if (IWPG == this.map[4]) {
                    pathFile = "./data/wall/wall_IWPG.obj";
                } else if (END == this.map[4]) {
                    pathFile = "./data/wall/wall_end.obj";

                }

                wall = new WallTexturized(map, this, pathFile, drawable);
                octree.add(wall);
                addChild(wall);
                walls.add(wall);
            }
        } catch (FileNotFoundException fe) {
            fe.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    @Override
    public void drawing() {
        Set<Model> set = octree.potencialCollisions(pivot);
        for (Model m : set) {
            m.draw();
        }
    }

    public void setPosition(Model pivot) {
        this.pivot = pivot;
    }

    public float getLength() {
        return this.length;
    }

    public float getHeight() {
        return this.height;
    }

    public float getWidth() {
        return this.width;
    }

    public int getPartitions() {
        return this.partitions;
    }

    public int getNumWalls() {
        return this.numWalls;
    }

    public ArrayList<WallTexturized> getWalls() {
        return walls;
    }

    public ArrayList<Floor> getFloors() {
        return floors;
    }

    public Vec4f getFront() {
        return new Vec4f(0, 0, 1);
    }

    public Vec4f getViewUp() {
        return new Vec4f(0, 1, 0);
    }

    public Vec4f getInitialPosition() {
        return new Vec4f((-getLength() / 2.0f) - (getLength() / (0.5f * getPartitions())),
                -getHeight() / 2,
                (-getWidth() / 2.0f) - (getWidth() / (0.5f * getPartitions())));
    }

    public Octree getOctree() {
        return octree;
    }

    @Override
    public void draw() {
        GL gl = drawable.getGL();

        gl.glMatrixMode(GL.GL_MODELVIEW);
        gl.glPushMatrix();

        gl.glMultMatrixf(modelview, 0);

        drawing();
        if (drawCollisionMask) {
            drawCollisionMask();
        }

        floors.get(0).draw();

        if (!Game.debugger) {
            floors.get(1).draw();
        }

        gl.glPopMatrix();
    }
}
