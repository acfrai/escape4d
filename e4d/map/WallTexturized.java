package e4d.map;

import com.sun.opengl.util.GLUT;
import e4d.object.Model;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;

public class WallTexturized extends Model {

    private int i;
    private int j;
    private int k;
    private int l;
    private int s1;
    private int s2;
    
    public static final int NONE = 0;
    public static final int TOP = 1;
    public static final int LEFT = 2;

    private Maze maze;
   
    public WallTexturized(int[] map, Maze maze, String file, GLAutoDrawable drawable) {
        super(file, drawable, 0);
        
        this.i = map[0];
        this.j = map[1];
        this.k = map[2];
        this.l = map[3];
        this.s1 = map[4];
        this.s2 = map[5];

        this.maze = maze;
        
        translate((i - maze.getPartitions() / 2) * (2 * maze.getLength()) / (float) maze.getPartitions(),
                ((j == 1) ? (maze.getHeight() / 2.0f) : (-maze.getHeight() / 2.0f)),
                (k - maze.getPartitions() / 2) * (2 * maze.getWidth()) / (float) maze.getPartitions());
        
        if (TOP == l) {
            translate((maze.getLength() / maze.getPartitions()), 0.0f, 0.0f);
            scale(maze.getLength() / maze.getPartitions(), maze.getHeight() / 2, (maze.getWidth() / maze.getPartitions()) * 0.1f);
            rotate(90.0f, 0.0f, 0.0f, 1.0f);
        } else if (LEFT == l) {
            translate(0.0f, 0.0f, (maze.getWidth() / maze.getPartitions()));
            rotate(90.0f, 0.0f, 1.0f, 0.0f);
            scale(maze.getWidth() / maze.getPartitions(), maze.getHeight() / 2, (maze.getWidth() / maze.getPartitions()) * 0.1f);
            rotate(90.0f, 0.0f, 0.0f, 1.0f);    
        } else if (NONE == l) {
            scale(0.0f, 0.0f, 0.0f);
        }
        
//        Pivot pivot = new Pivot(.5f, drawable);
//        pivot.translate(0, (j == 1 ? -1 : 1)*maze.getHeight(), 0);
//        addChild(pivot);
        
        size = 0.5f;
    }

    public int getI() {
        return i;
    }

    public int getJ() {
        return j;
    }

    public int getK() {
        return k;
    }

    public int getL() {
        return l;
    }

    public void setI(int i) {
        this.i = i;
    }

    public void setJ(int j) {
        this.j = j;
    }

    public void setK(int k) {
        this.k = k;
    }

    public void setL(int l) {
        this.l = l;
    }

    void setS1(int s1) {
        this.s1 = s1;
    }

    void setS2(int s2) {
        this.s2 = s2;
    }
}
