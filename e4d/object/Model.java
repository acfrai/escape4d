package e4d.object;

import com.sun.opengl.util.GLUT;
import e4d.utils.Vec4f;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;

public class Model {

    public static boolean drawCollisionMask = false;
    
    public static final int ON_RIGHT = 0;
    public static final int ON_LEFT = 1;
    public static final int ON_TOP = 2;
    public static final int ON_BOTTOM = 3;
    public static final int ON_NEAR = 4;
    public static final int ON_FAR = 5;
    
    protected GLAutoDrawable drawable;
    protected float modelview[] = new float[16];
    protected ArrayList<Model> children = new ArrayList<Model>();
    protected Model father = null;
    protected JWavefrontModel model = null;
    
    protected float size;
    private long lastMillis = 0;

    public Model(GLAutoDrawable drawable) {
        init(drawable);
    }

    public Model(String filename, GLAutoDrawable drawable, int auxSide) {
        init(drawable);

        try {
            model = new JWavefrontModel(new File(filename));
            model.unitize();
            model.facetNormals();
            if(auxSide == 1) model.reverseWinding();
            model.vertexNormals(90);
            model.dump(true);
            model.compile(drawable, JWavefrontModel.WF_MATERIAL | JWavefrontModel.WF_TEXTURE | JWavefrontModel.WF_SMOOTH);
        } catch (IOException ex) {
            Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void init(GLAutoDrawable drawable) {
        this.drawable = drawable;
        GL gl = drawable.getGL();

        // Inicializa a modelview do objeto
        gl.glMatrixMode(GL.GL_MODELVIEW);
        gl.glPushMatrix();

        gl.glLoadIdentity();
        gl.glGetFloatv(GL.GL_MODELVIEW_MATRIX, modelview, 0);

        gl.glPopMatrix();

        size = 1.0f;
        clearTicks();
    }

    protected void drawing() {
        if (model != null) {
            model.draw(drawable);
        } else {
            GLUT glut = new GLUT();
            glut.glutWireTeapot(1.0f);
        }
    }

    public void addChild(Model child) {
        children.add(child);
        child.addFather(this);
    }

    public void draw() {
        GL gl = drawable.getGL();
        
        gl.glMatrixMode(GL.GL_MODELVIEW);
        gl.glPushMatrix();

        gl.glMultMatrixf(modelview, 0);

        drawing();
        if (drawCollisionMask) {
            drawCollisionMask();
        }

        Iterator<Model> iter = children.iterator();
        while (iter.hasNext()) {
            Model child = iter.next();
            child.draw();
        }

        gl.glPopMatrix();
    }

    public void rotate(float angle, Vec4f vec) {
        rotate(angle, vec.x(), vec.y(), vec.z());
    }
    
    public void rotate(float angle, float x, float y, float z) {
        GL gl = drawable.getGL();

        gl.glMatrixMode(GL.GL_MODELVIEW);

        gl.glPushMatrix();

        gl.glLoadIdentity();
        gl.glMultMatrixf(modelview, 0);
        gl.glRotatef(angle, x, y, z);
        gl.glGetFloatv(GL.GL_MODELVIEW_MATRIX, modelview, 0);

        gl.glPopMatrix();
    }

    public void scale(float s) {
        scale(s, s, s);
    }
    
    public void scale(float x, float y, float z) {
        GL gl = drawable.getGL();

        gl.glMatrixMode(GL.GL_MODELVIEW);

        gl.glPushMatrix();

        gl.glLoadIdentity();
        gl.glMultMatrixf(modelview, 0);
        gl.glScalef(x, y, z);
        gl.glGetFloatv(GL.GL_MODELVIEW_MATRIX, modelview, 0);

        gl.glPopMatrix();
    }

    public void translate(Vec4f translation) {
        translate(translation.x(), translation.y(), translation.z());
    }
    
    public void translate(float x, float y, float z) {
        GL gl = drawable.getGL();

        gl.glMatrixMode(GL.GL_MODELVIEW);

        gl.glPushMatrix();

        gl.glLoadIdentity();
        gl.glMultMatrixf(modelview, 0);
        gl.glTranslatef(x, y, z);
        gl.glGetFloatv(GL.GL_MODELVIEW_MATRIX, modelview, 0);

        gl.glPopMatrix();
    }

    public void multMatrix(float[] matrix, int offset) {
        GL gl = drawable.getGL();

        gl.glMatrixMode(GL.GL_MODELVIEW);

        gl.glPushMatrix();

        gl.glLoadIdentity();
        gl.glMultMatrixf(modelview, 0);
        gl.glMultMatrixf(matrix, offset);
        gl.glGetFloatv(GL.GL_MODELVIEW_MATRIX, modelview, 0);

        gl.glPopMatrix();
    }

    public void reset() {
        GL gl = drawable.getGL();

        gl.glMatrixMode(GL.GL_MODELVIEW);

        gl.glPushMatrix();

        gl.glLoadIdentity();
        gl.glGetFloatv(GL.GL_MODELVIEW_MATRIX, modelview, 0);

        gl.glPopMatrix();
    }

    protected long getTicks() {
        long millis = System.currentTimeMillis();
        return millis - lastMillis;
    }

    protected void clearTicks() {
        lastMillis = System.currentTimeMillis();
    }

    protected void printModelView() {
        for (int i = 0; i < 16; i++) {
            System.out.print(modelview[i] + "f, ");
        }
        System.out.println();
    }

    public Vec4f getCenter() {
        GL gl = drawable.getGL();

        Vec4f center;
        float[] matrix = new float[16];

        gl.glMatrixMode(GL.GL_MODELVIEW);
        gl.glPushMatrix();

        setResultMatrix();
        gl.glGetFloatv(GL.GL_MODELVIEW_MATRIX, matrix, 0);

        gl.glPopMatrix();

        // calculo otimizado
        center = new Vec4f(matrix[12] / matrix[15], 
                           matrix[13] / matrix[15], 
                           matrix[14] / matrix[15]);

        return center;
    }

    protected void addFather(Model object) {
        if (father == null) {
            father = object;
        }
    }

    protected void setResultMatrix() {
        GL gl = drawable.getGL();

        if (father == null) {
            gl.glLoadIdentity();
            gl.glMultMatrixf(modelview, 0);
        } else {
            father.setResultMatrix();
            gl.glMultMatrixf(modelview, 0);
        }
    }

    public float getSize() {
        GL gl = drawable.getGL();
        float[] matrix = new float[16];
        float scale = 0.0f;

        gl.glMatrixMode(GL.GL_MODELVIEW);
        gl.glPushMatrix();

        this.setResultMatrix();
        gl.glGetFloatv(GL.GL_MODELVIEW_MATRIX, matrix, 0);

        gl.glPopMatrix();
        
        Vec4f[] axis = new Vec4f[3];
        Vec4f center = getCenter();
        
        axis[0] = new Vec4f(size, 0.0f, 0.0f);
        axis[1] = new Vec4f(0.0f, size, 0.0f);
        axis[2] = new Vec4f(0.0f, 0.0f, size);
        
        for(int i = 0; i < 3; i++) {
            axis[i] = axis[i].multMatrix4(matrix);
            float s = axis[i].sub(center).magnitude();
            scale = scale > s ? scale : s;
        }
        
        return scale;
    }

    public boolean isOn(int side, Vec4f vec) {
        switch (side) {
            case ON_RIGHT:
                if (getCenter().x() + getSize() >= vec.x()) {
                    return true;
                }
                break;
            case ON_LEFT:
                if (getCenter().x() - getSize() <= vec.x()) {
                    return true;
                }
                break;
            case ON_TOP:
                if (getCenter().y() + getSize() >= vec.y()) {
                    return true;
                }
                break;
            case ON_BOTTOM:
                if (getCenter().y() - getSize() <= vec.y()) {
                    return true;
                }
                break;
            case ON_FAR:
                if (getCenter().z() + getSize() >= vec.z()) {
                    return true;
                }
                break;
            case ON_NEAR:
                if (getCenter().z() - getSize() <= vec.z()) {
                    return true;
                }
                break;
        }

        for (Model child : children) {
            if (child.isOn(side, vec)) {
                return true;
            }
        }

        return false;
    }

    public void drawCollisionMask() {
        GL gl = drawable.getGL();
        Vec4f center = getCenter();

        gl.glMatrixMode(GL.GL_MODELVIEW);
        
        gl.glPushMatrix();
        gl.glLoadIdentity();
        gl.glTranslatef(center.x(), center.y(), center.z());
        (new GLUT()).glutWireSphere(getSize(), 20, 20);
        gl.glPopMatrix();
    }
    
    public boolean collides(Model other) {
        if(getCenter().distance(other.getCenter()) < (getSize() + other.getSize())) {
            return true;
        }
        return false;
    }
    
    public float[] getModelview() {
        float[] result = new float[16];
        System.arraycopy(modelview, 0, result, 0, 16);
        return result;
    }
    
    public void reverseNormals()
    {
        model.reverseWinding();
    }
}