package e4d.object;

import java.util.ArrayList;
import java.util.Iterator;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;

public class AnimatedModel extends Model {

    protected ArrayList<AnimatedModel> animatedChildren = new ArrayList<AnimatedModel>();
    protected ArrayList<Integer> nextStates = new ArrayList<Integer>();
    protected int state;

    public AnimatedModel(GLAutoDrawable drawable) {
        super(drawable);

        init();
    }

    public AnimatedModel(String filename, GLAutoDrawable drawable, int auxSide) {
        super(filename, drawable, auxSide);

        init();
    }

    private void init() {
        state = 0;
    }

    protected void animation() {
    }

    public void addChild(AnimatedModel child) {
        animatedChildren.add(child);
        children.add(child);
        child.addFather(this);
    }

    @Override
    public void draw() {
        
        GL gl = drawable.getGL();

        gl.glMatrixMode(GL.GL_MODELVIEW);
        gl.glPushMatrix();

        gl.glMultMatrixf(modelview, 0);

        animation();
        if(drawCollisionMask)
                drawCollisionMask();
        drawing();

        Iterator<Model> iter = children.iterator();
        while (iter.hasNext()) {
            Model child = iter.next();
            child.draw();
        }

        gl.glPopMatrix();
    }

    public void requestState(int state) {
        nextStates.add(state);

        Iterator<AnimatedModel> iter = animatedChildren.iterator();
        while (iter.hasNext()) {
            AnimatedModel child = iter.next();
            child.requestState(state);
        }
    }

    protected void transition(int stateFrom, int stateTo) {
        if (stateFrom > stateTo) {
            state = (stateFrom | stateTo) + 1;
        } else {
            state = (stateFrom | stateTo);
        }
    }
}
