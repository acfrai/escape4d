package e4d.ninja;

import e4d.core.Game;
import e4d.core.Ninja;
import e4d.object.AnimatedModel;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;

public class NinjaLowerLeg extends AnimatedModel {

    private int side;
    private int wait;
    private float time_S2R = 1000.0f; 

    public NinjaLowerLeg(GLAutoDrawable drawable, int side, int auxSide) {
        super("./data/big_man_lower_leg.obj", drawable, auxSide);
        
        this.side = side;
        
        float[] lleg_modelview = {1.9499992f, 0.0f, 0.0f, 0.0f, 0.0f, 1.9499992f, 0.0f, 0.0f, 0.0f, 0.0f, 1.9499992f, 0.0f, 0.74999917f, -3.0050213f, 0.18999995f, 1.0f};
        multMatrix(lleg_modelview, 0);
    }
   @Override
    protected void animation() {

        GL gl = drawable.getGL();
        long ticks = getTicks();

        switch (state) {
            case Game.STATE_STAND:
                if (!nextStates.isEmpty()) {
    
                    transition(state, nextStates.remove(0));
                }
                break;

            case Game.STATE_STAND2RUNNING:
                state = Game.STATE_RUNNING;
                if (side == Ninja.RIGHT) {
                    wait = (int) time_S2R/2;
                }
                clearTicks();
                break;

            case Game.STATE_RUNNING:
            case Game.STATE_RUNNING2STAND:
                float degrees = 0;

                if (ticks < wait) {
                    return;
                } else if (wait > 0) {
                    wait = -1;
                    clearTicks();
                    return;
                }

                if (ticks <= time_S2R*0.45f) {
                    degrees = 90.0f * ((ticks) / (time_S2R*0.45f));

                } else if (ticks <= time_S2R*0.9f) {
                    degrees = 90.0f - 90.0f * ((ticks - (time_S2R*0.45f)) / (time_S2R*0.9f - time_S2R*0.45f));

                } else if (ticks <= time_S2R*0.95f) {
                    degrees = 0.0f * ((ticks - (time_S2R*0.9f)) / (time_S2R*0.95f - time_S2R*0.9f));

                } else if (ticks <= time_S2R) {
                    degrees = 0.0f - 0.0f * ((ticks - (time_S2R*0.95f)) / (time_S2R*1.0f - time_S2R*0.95f));

                } else {
                    clearTicks();
                    if(state == Game.STATE_RUNNING2STAND) {
                        state = Game.STATE_STAND;
                        return;
                    }
                }

                gl.glTranslatef(0.0f, 0.85f, 0.0f);
                gl.glRotatef(degrees, 1, 0, 0);
                gl.glTranslatef(0.0f, -0.85f, 0.0f);

                if (!nextStates.isEmpty()) {
                    int lState = state;
                    transition(state, nextStates.remove(0));
                    while(state == lState && !nextStates.isEmpty()) {
                        transition(state, nextStates.remove(0));
                    }
                }

                break;

            default:
                if (!nextStates.isEmpty()) {
                    state = nextStates.remove(0);
                }
        }
    }


}