package e4d.ninja;

import e4d.core.Game;
import e4d.core.Ninja;
import e4d.object.AnimatedModel;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;

public class NinjaArm extends AnimatedModel {

    private final NinjaLowerArm lowerArm = new NinjaLowerArm(drawable);
    private float time_S2R = 1000.0f;
    private float[] rel_S2R = {0.48f, 0.80f, 0.98f};
    private int wait;
    private int side;

    public NinjaArm(GLAutoDrawable drawable, int side, int auxSide) {
        super("./data/big_man_upper_arm.obj", drawable, auxSide);

        this.side = side;

        float mirror[] = {-1.0f, 0.0f, 0.0f, 0.0f,
            0.0f, 1.0f, 0.0f, 0.0f,
            0.0f, 0.0f, 1.0f, 0.0f,
            0.0f, 0.0f, 0.0f, 1.0f};

        if (side == Ninja.RIGHT) {
            multMatrix(mirror, 0);
        }

        float[] arm_modelview = {0.7750002f, 0.0f, 0.0f, 0.0f,
            0.0f, 0.7750002f, 0.0f, 0.0f,
            0.0f, 0.0f, 0.7750002f, 0.0f,
            0.6949995f, 0.19499996f, -0.12500001f, 1.0f};
        multMatrix(arm_modelview, 0);

        addChild(lowerArm);
        size = 0.6f;
    }

    @Override
    protected void animation() {

        GL gl = drawable.getGL();
        long ticks = getTicks();

        switch (state) {
            case Game.STATE_STAND:
                if (!nextStates.isEmpty()) {
   
                    transition(state, nextStates.remove(0));
                }
                break;

            case Game.STATE_STAND2RUNNING:
                state = Game.STATE_RUNNING;
                if (side == Ninja.RIGHT) {
                    wait = (int) time_S2R / 2;
                }
                clearTicks();
                break;

            case Game.STATE_RUNNING:
            case Game.STATE_RUNNING2STAND:
                float degrees = 0;

                if (ticks < wait) {
                    if (!nextStates.isEmpty()) {
                        transition(state, nextStates.remove(0));
                    }
                    if (state == Game.STATE_RUNNING2STAND) {
                        state = Game.STATE_STAND;
                        return;
                    }
                    return;
                } else if (wait > 0) {
                    wait = -1;
                    clearTicks();
                    return;
                }

                if (ticks <= time_S2R * rel_S2R[0]) {

                    degrees = -10.0f * ((ticks) / (time_S2R * rel_S2R[0]));

                } else if (ticks <= time_S2R * rel_S2R[1]) {
                    degrees = -10.0f + 20.0f * ((ticks - (time_S2R * rel_S2R[0])) / (time_S2R * rel_S2R[1] - time_S2R * rel_S2R[0]));


                } else if (ticks <= time_S2R * rel_S2R[2]) {
                    degrees = 10 - 10.0f * ((ticks - (time_S2R * rel_S2R[1])) / (time_S2R * rel_S2R[2] - time_S2R * rel_S2R[1]));

                } else {
                    clearTicks();
                    if (state == Game.STATE_RUNNING2STAND) {
                        state = Game.STATE_STAND;
                        return;
                    }
                }

                gl.glTranslatef(0.0f, 0.25f, 0.0f);
                gl.glRotatef(degrees, 1, 0, 0);
                gl.glTranslatef(0.0f, -0.25f, 0.0f);

                if (!nextStates.isEmpty()) {
                    int lState = state;
                    transition(state, nextStates.remove(0));
                    while(state == lState && !nextStates.isEmpty()) {
                        transition(state, nextStates.remove(0));
                    }
                }

                break;

            default:
                if (!nextStates.isEmpty()) {
                    state = nextStates.remove(0);
                }
        }
    }


}
