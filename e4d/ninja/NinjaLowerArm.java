package e4d.ninja;

import e4d.core.Game;
import e4d.core.Ninja;
import e4d.object.AnimatedModel;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;

public class NinjaLowerArm extends AnimatedModel {

    private int side;
    private int wait;
    private float time_S2R = 1500.0f;

    public NinjaLowerArm(GLAutoDrawable drawable) {
        super("./data/big_man_lower_arm.obj", drawable, 0);

        float[] larm_modelview = {1.1699998f, 0.0f, 0.0f, 0.0f,
            0.0f, 1.1699998f, 0.0f, 0.0f,
            0.0f, 0.0f, 1.1699998f, 0.0f,
            0.21999992f, -1.1849991f, 0.10500002f, 1.0f};

        multMatrix(larm_modelview, 0);
        size = 0.5f;
    }

    @Override
    protected void animation() {

        GL gl = drawable.getGL();
        long ticks = getTicks();

        switch (state) {
            case Game.STATE_STAND:
                if (!nextStates.isEmpty()) {

                    transition(state, nextStates.remove(0));
                }
                break;

            case Game.STATE_STAND2RUNNING:
                state = Game.STATE_RUNNING;
                if (side == Ninja.RIGHT) {
                    wait = (int) time_S2R / 2;
                }
                clearTicks();
                break;

            case Game.STATE_RUNNING:
            case Game.STATE_RUNNING2STAND:
                float degrees = 0;

                if (ticks < wait) {
                    return;
                } else if (wait > 0) {
                    wait = -1;
                    clearTicks();
                    return;
                }

                if (ticks <= time_S2R * 0.45f) {
                    degrees = 90.0f * ((ticks) / (time_S2R * 0.45f));

                } else if (ticks <= time_S2R * 0.9f) {
                    degrees = 90.0f - 110.0f * ((ticks - (time_S2R * 0.45f)) / (time_S2R * 0.9f - time_S2R * 0.45f));


                    degrees = 0.0f * ((ticks - (time_S2R * 0.9f)) / (time_S2R * 0.95f - time_S2R * 0.9f));



                } else {
                    clearTicks();
                    if (state == Game.STATE_RUNNING2STAND) {
                        state = Game.STATE_STAND;
                        return;
                    }
                }



                if (!nextStates.isEmpty()) {
                    int lState = state;
                    transition(state, nextStates.remove(0));
                    while (state == lState && !nextStates.isEmpty()) {
                        transition(state, nextStates.remove(0));
                    }
                }

                break;

            default:
                if (!nextStates.isEmpty()) {
                    state = nextStates.remove(0);
                }
        }
    }

}