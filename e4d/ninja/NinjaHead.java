package e4d.ninja;

import e4d.object.AnimatedModel;
import javax.media.opengl.GLAutoDrawable;

public class NinjaHead extends AnimatedModel {

    public NinjaHead(GLAutoDrawable drawable) {
        super("./data/big_man_head.obj", drawable, 0);

        float[] head_modelview = {0.30000067f, 0.0f, 0.0f, 0.0f,
            0.0f, 0.30000067f, 0.0f, 0.0f,
            0.0f, 0.0f, 0.30000067f, 0.0f,
            0.0f, 1.0949992f, 0.015f, 1.0f};
        multMatrix(head_modelview, 0);

    }
}