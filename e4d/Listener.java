package e4d;

import e4d.core.Game;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;

class Listener implements GLEventListener, KeyListener {
    
    private Set<Integer> keys = new HashSet<Integer>();
    
    @Override
    public void init(GLAutoDrawable drawable) {
        Game game = Game.getInstance();
        game.init(drawable);
        
    }
    
    @Override
    public void display(GLAutoDrawable drawable) {
        Game game = Game.getInstance();
        for (int key : keys) {
            game.addKey(key);
        }
        game.update(drawable);
    }
    
    @Override
    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        Game game = Game.getInstance();
        game.reshape(drawable, x, y, width, height);
    }
    
    @Override
    public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
    }
    
    @Override
    public void keyTyped(KeyEvent e) {
    }
    
    @Override
    public void keyPressed(KeyEvent e) {
        int code = e.getKeyCode();
        Game game = Game.getInstance();
        
        switch (code) {
            case KeyEvent.VK_W:
            case KeyEvent.VK_A:
            case KeyEvent.VK_S:
            case KeyEvent.VK_D:
                keys.add(e.getKeyCode());
                break;
            
            default:
                game.addKey(code);
                break;
        }
        
    }
    
    @Override
    public void keyReleased(KeyEvent e) {
        keys.remove(e.getKeyCode());
    }
}
